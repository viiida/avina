import React  from 'react';
import { useForm } from "react-hook-form";


function Modal(props) {

    const { register,handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => {
        props.handleSubmit(data)
       
    };

    return (<>
        {props.isShow && (
            <div className='main__modal'>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <input  {...register("firstname")} />
                    {errors.firstname && <span>This field is required</span>}

                    <input {...register("lastname", { required: true })} />
                    {errors.lastname && <span>This field is required</span>}

                    <input type="submit"  />
                   
                </form>



            </div>
        )}

    </>

    )
}
export default Modal