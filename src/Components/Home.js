import React from 'react';
import Main from './Main';
import SideBar from './SideBar';
import Nav from './Nav';

function Home() {
    return (
        <div>
            <Nav />
            <SideBar />
            <Main />
        </div>
    )
}
export default Home