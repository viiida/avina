import React, { useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Modal from './Modal';

function createData(firstname, lastname, phoneNumber) {
    return { firstname, lastname, phoneNumber };
}


function Main() {
    const [rows, setRows] = useState([])

    const [isShow, setIsShow] = useState(false)
    const toggleModal = () => {
        setIsShow(!isShow);
    }

    const handleClick = data => {
        setRows([...rows, createData(data.firstname, data.lastname, "09167005033")])
    };
    return (

        <Container maxWidth="lg" >

            <div>
                {/* button */}
                <div className='main__new-contact'>
                    <Button variant="contained" onClick={toggleModal}>New Contact</Button>

                </div>
                <div className='main__table'>
                    <Modal isShow={isShow} handleSubmit={handleClick} />

                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Firstname</TableCell>
                                    <TableCell align="right">Lastname</TableCell>
                                    <TableCell align="right">Phone number</TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.map((row) => {
                                    console.log(rows)
                                    return (
                                        <TableRow
                                            key={row.firstname}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell component="th" scope="row">
                                                {row.firstname}
                                            </TableCell>
                                            <TableCell align="right">{row.lastname}</TableCell>
                                            <TableCell align="right">{row.phoneNumber}</TableCell>

                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>

            </div>

        </Container>
    );
}

export default Main;