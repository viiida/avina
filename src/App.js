import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import AboutUs from './Components/AboutUs';
import Home from './Components/Home';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/about/' element={<AboutUs />} />
          <Route path='/' element={<Home />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
